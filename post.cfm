<cfparam name="form.btncomment" default="">
<cfparam name="form.username" default="">
<cfparam name="form.useremail" default="">
<cfparam name="form.commenttext" default="">
<cfparam name="form.datetime" default="">
<cfparam name="url.PostID" default="">

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Clean Blog - Start Bootstrap Theme</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
    <link href="css/clean-blog.min.css" rel="stylesheet">
  </head>
  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.cfm">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.cfm">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="post.cfm">Post</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.cfm">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

  <cfoutput>

   <cfif url.PostID gt 0> 
 


        <cfquery name="post" datasource="#application.db#">    
           SELECT * FROM posts 
           WHERE PostID = <cfqueryparam cfsqltype="cf_sql_varchar"  value="#url.PostID#"> 
        </cfquery>

        <cfquery name="user" datasource="#application.db#">    
                SELECT * FROM admin 
                WHERE AdminID = <cfqueryparam cfsqltype="cf_sql_varchar"  value="#session.AdminID#"> 
        </cfquery> 

         <cfset date = DateFormat(post.PostDate, "dd mmmm yyyy")>

        <!-- Page Header -->
        <header class="masthead" style="background-image: url('img/post/#post.PostImage#')">
          <div class="overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 col-md-10 mx-auto">
                <div class="post-heading">
                  <h1>#post.PostTitle#</h1>
                  <h2 class="subheading">#post.PostSubTitle#</h2>
                  <span class="meta">Posted by
                    <a href="##">#user.Firstname# #user.Surname#</a>
                    on #date#</span>
                </div>
              </div>
            </div>
          </div>
        </header>
        <!-- Post Content -->
        <article>
          <div class="container">
            <div class="row">

              <div class="col-lg-8 col-md-10 mx-auto">   

                  <div class="post-preview"> 
                    <h2 class="section-heading">#post.PostTitle#</h2>
                    </br>
                    <p>#post.PostText#</p>
                      </br>
                    <p class="post-meta">Posted on
                       #date#</p>
                    </div>    
                    <p>by #user.Firstname# #user.Surname# </p>
           
                  <form  method="POST" action="?postID=#url.PostID#" enctype="multipart/form-data">
                    <h2 class="subheading">Comment</h2>
                      <div class="form-group">
                          <div class="form-row"> 
                            <div class="col-md-6">
                              <input class="form-control" id="username" name="username" required="true" type="text" aria-describedby="nameHelp" placeholder="Name...">
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="form-row"> 
                            <div class="col-md-6">
                              <input class="form-control" id="useremail" name="useremail" required="true" type="text" aria-describedby="nameHelp" placeholder="Email.....">
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="form-row">
                            <div class="col-md-6">
                              <textarea class="form-control" id="commentText " name="commenttext" required="true" type="text" rows="3" cols="60" placeholder="Comments......."></textarea>
                            </div>
                          </div>
                        </div>
                    <input type="submit" name="btncomment" class="btn btn-primary" value="submit" > 
                    </br>
                    </br>
                    
                    <p class="post-meta">Comments</p>  

                    <cfquery name="allcomments" datasource="#application.db#"> 
                      SELECT UserName,UserEmail,CommentDate,CommentText 
                      FROM comments 
                      WHERE PostID = <cfqueryparam value="#url.PostID#">
                   </cfquery>
  
                    <cfloop query="allcomments"> 
                    <cfset date = DateFormat(allcomments.CommentDate, "dd mmmm yyyy")>
                    <div class="post-preview"> 
                        <h2 class="post-meta">
                          #allcomments.UserName#
                        </h2>
                        <h3 class="post-meta">
                          #allcomments.CommentText#
                        </h3>
                      </a>
                        <p class="post-meta">Posted on
                          #date#</p>
                    </div>
                    <hr>
                    </cfloop>
                 </form>

                 <cfif form.btncomment gt ""> 

                  <cfset datetime = DateFormat(now(), "yyyy-mm-dd")>             
                      <cfquery name="comment" datasource="#application.db#">
                          INSERT INTO comments(PostID,UserName,UserEmail,CommentDate,CommentText)
                          VALUES (<cfqueryparam value="#url.PostID#" cfsqltype="cf_sql_varchar" />, 
                                  <cfqueryparam value="#form.username#" cfsqltype="cf_sql_varchar" />,
                                  <cfqueryparam value="#form.useremail#" cfsqltype="cf_sql_varchar" />,     
                                  <cfqueryparam value="#datetime#" cfsqltype= "cf_sql_varchar" />, 
                                  <cfqueryparam value="#form.commenttext#" cfsqltype= "cf_sql_varchar" />)            
                      </cfquery> 
                      <cfset  form.btncomment = ""> 
                  </cfif>
              </div>
            </div>
          </div>
        </article>
        <hr>
  <cfelse>
      <h3 style="background-color:rgba(255, 99, 71, 0.5);color:white;">Error,Page not found!.</h3>      
  </cfif>
  </cfoutput>
    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <ul class="list-inline text-center">
              <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
            </ul>
            <p class="copyright text-muted">Copyright &copy; Blogah 2018</p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/clean-blog.min.js"></script>
  </body>
</html>


  <script type="JavaScript">

/*    $("#btncomment").click(function()
    {     
         cfset datetime = DateFormat(now(), "yyyy-mm-dd")> 
         cfset db = application.db>

         var postid  = url.PostID;
         var username = document.getElementById('username');
         var useremail = document.getElementById('useremail');
         var commentdate = datetime;
         var commenttext = document.getElementById('commentText');

         $(ajax({
                url: "newcomment.cfm",
                type : "POST",
                success: function(result)
                {
                     
                },
                error: function(result)
                {
                     alert(Error error!);
                }
          });)
    }*/
    
  </script>
