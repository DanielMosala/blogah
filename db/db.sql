----------------------------------------------------------
--                CREATE ADMIN TABLE
----------------------------------------------------------
CREATE TABLE `admin` (
  `AdminID` int(11) NOT NULL AUTO_INCREMENT,
  `EmailAddress` varchar(100) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Firstname` varchar(255) NOT NULL,
  `Surname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AdminID`),
  UNIQUE KEY `EmailAddress_UNIQUE` (`EmailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
----------------------------------------------------------
----------------------------------------------------------


----------------------------------------------------------
--                 CREATE POSTS TABLE
----------------------------------------------------------
CREATE TABLE `posts` (
  `PostID` int(11) NOT NULL AUTO_INCREMENT,
  `AdminID` int(11) NOT NULL,
  `PostTitle` varchar(100) NOT NULL,
  `PostDate` datetime NOT NULL,
  `PostText` text NOT NULL,
  `PostImage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PostID`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
----------------------------------------------------------
----------------------------------------------------------


----------------------------------------------------------
--                 CREATE COMMENTS TABLE
----------------------------------------------------------
CREATE TABLE `comments` (
  `CommentID` int(45) NOT NULL AUTO_INCREMENT,
  `PostID` int(11) NOT NULL,
  `UserName` varchar(255) NOT NULL,
  `UserEmail` varchar(255) NOT NULL,
  `CommentDate` datetime NOT NULL,
  `CommentText` text NOT NULL,
  PRIMARY KEY (`CommentID`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
----------------------------------------------------------
----------------------------------------------------------

