<cfparam name="form.btnsaveedit" default="">
<cfparam name="form.PostID" default="">
<cfparam name="form.posttitle" default="">
<cfparam name="form.postsubtitle" default="">
<cfparam name="form.posttext" default="">
<cfparam name="form.datetime" default="">
<cfset datetime = DateFormat(now(),"yyyy-mm-dd")> 

<!DOCTYPE html>
<html lang="en">

<cfinclude template="header.cfm">

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.cfm">Posts</a>
        </li>
        <li class="breadcrumb-item active">Edit</li>
      </ol>
      <div class="row">
     <div class="col-12">

      <cfquery name="post" datasource="#application.db#">    
          SELECT PostTitle,PostSubTitle,PostText,PostImage FROM posts 
          WHERE PostID = <cfqueryparam value="#url.PostID#">
      </cfquery>

     <cfif form.btnsaveedit gt "">
            <cfset filename = "">
            <cfif fileUpload gt "">
                <cffile action="upload" fileField="fileUpload" accept="image/jpg,image/gif,image/png" destination="#application.filepath#/post" nameconflict="makeunique">
                       <cfset filename = cffile.serverfile> 
            </cfif>  
             <cfset datetime = DateFormat(now(), "yyyy-mm-dd")>
             <cfquery name="post" datasource="#application.db#"> 
                  UPDATE posts
                  SET 
                  PostTitle = <cfqueryparam value="#form.posttitle#" cfsqltype="cf_sql_varchar" />,
                  PostSubTitle = <cfqueryparam value="#form.postsubtitle#" cfsqltype="cf_sql_varchar" />,
                  PostDate = <cfqueryparam value="#datetime#" cfsqltype= "cf_sql_varchar" />, 
                  PostText = <cfqueryparam value="#form.posttext#" cfsqltype= "cf_sql_varchar" />
                  <cfif filename gt "">
                  ,PostImage = <cfqueryparam value="#filename#" cfsqltype= "cf_sql_varchar" />
                  </cfif>
                  where PostID = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.PostID#"/>
              </cfquery> 
        
              <cflocation url="posts.cfm?Edit=true&PostID=#PostID#" addtoken="false"> 

              <cfquery name="post" datasource="#application.db#">    
                SELECT PostTitle,PostSubTitle,PostText,PostImage FROM posts 
                WHERE PostID = <cfqueryparam value="#url.PostID#">
              </cfquery>
      </cfif>
       
  <cfoutput>
    <form  method="POST" enctype="multipart/form-data">

      <h1>Edit Post </h1>
        <div class="form-group">
            <div class="form-row"> 
              <div class="col-md-6">
                <label for="exampleInputPostTitle">Post Title </label>
                <input class="form-control" id="exampleInputPostTitle" value="#post.PostTitle#" name="posttitle" type="text" aria-describedby="nameHelp" required="true" placeholder="Post title">
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row"> 
              <div class="col-md-6">
                <label for="exampleInputPostTitle">Post Sub Title </label>
                <input class="form-control" id="exampleInputPostSubTitle" value="#post.PostSubTitle#" name="postsubtitle" type="text" aria-describedby="nameHelp" required="true" placeholder="Post Sub title">
              </div>
            </div>
          </div>

          <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <label for="exampleInputImage ">Current image:</label>  
                  <img src="../img/post/#post.PostImage#" alt="">
                </div>
              </div>
          </div>

          <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <input  name="fileUpload" type="file" required="true" placeholder="Image" id="FileUpload">
         
                </div>
              </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPostText ">Post Text </label>
                <textarea class="form-control" id="exampleInputPostText" required="true" value="#post.posttext#" name="posttext" type="text" row="20" col="60">#post.posttext#</textarea>
              </div>
            </div>
          </div>
          <input type="submit" name="btnsaveedit" class="btn btn-primary" value="Update Post">  
        </form>
      </cfoutput>
        </div>
      </div>
    </div>
    <cfinclude template="footer.cfm">
  </div>
</body>
</html>
