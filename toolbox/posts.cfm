<cfparam name="form.btnedit" default="">
<cfparam name="form.btndelete" default="">
<cfparam name="url.delete" default="">
<cfparam name="url.PostID" default="">

<cfinclude template="header.cfm">
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="posts.cfm">Posts</a>
        </li>
        <li class="breadcrumb-item active">All</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Posts 
            <a href="post_new.cfm" class="pull-right btn btn-success">New Post &nbsp;<span class="fa fa-pencil"></span></a>
        </div>
        <div class="card-body">
        <div class="table-responsive">


     <cfoutput>
          <cfquery name="allposts" datasource="#application.db#">    
              SELECT * FROM posts        
          </cfquery> 

          <cfif url.PostID gt 0>
            <h3 style="background-color:lightgreen;color:white;">Post ID  #url.PostID#  is been updated!.</h3>
             <table class="table table-bordered" id="dataTable" name="posts" width="80%" cellspacing="0">
                <thead>
                  <tr>
                    <th>PostID </th>
                    <th>PostTitle </th>
                    <th>PostSubTitle </th>
                    <th>PostText </th>  
                    <th>PostDate </th>    
                    <th>PostImage </th>  
                  </tr>
                </thead> 
                <tbody>
                   <cfloop query="allposts">
                      <tr>
                         <td width="3%">#allposts.PostID#</td>
                         <td width="20%">#allposts.PostTitle#</td>
                         <td width="20%">#allposts.PostSubTitle#</td>
                         <td width="33%">#allposts.PostText#</td>
                         <td width="20%">#DateFormat(allposts.PostDate, "dd/mm/yyyy")#</td>
                         <td width="33%">#allposts.PostImage#</td> 
                         <td width="3%"><a href="edit.cfm?Edit=true&PostID=#PostID#">Edit</a> </td> 
                         <td width="3%"><a style="color:red;" href="posts.cfm?Delete=true&PostID=#PostID#">Delete</a></td>
                      </tr>
                   </cfloop> 
                </tbody>
              </table>
          <cfelse>
              <table class="table table-bordered" id="dataTable" name="posts" width="80%" cellspacing="0">
                <thead>
                  <tr>
                    <th>PostID </th>
                    <th>PostTitle </th>
                    <th>PostSubTitle </th>
                    <th>PostText </th>  
                    <th>PostDate </th>    
                    <th>PostImage </th>  
                  </tr>
                </thead> 
                <tbody>
                   <cfloop query="allposts">
                      <tr>
                         <td width="3%">#allposts.PostID#</td>
                         <td width="20%">#allposts.PostTitle#</td>
                         <td width="20%">#allposts.PostSubTitle#</td>
                         <td width="33%">#allposts.PostText#</td>
                         <td width="20%">#DateFormat(allposts.PostDate, "dd/mm/yyyy")#</td>
                         <td width="33%">#allposts.PostImage#</td> 
                         <td width="3%"><a href="edit.cfm?Edit=true&PostID=#PostID#">Edit</a> </td> 
                         <td width="3%"><a style="color:red;" href="posts.cfm?Delete=true&PostID=#PostID#">Delete</a></td>
                      </tr>
                   </cfloop> 
                </tbody>
              </table>
          </cfif>

           <cfif url.Delete gt "">
             <cfquery name="deletepost" datasource="#application.db#">
               delete from posts
               where PostID = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.PostID#"/>
             </cfquery> 
              <cflocation url="posts.cfm" addtoken="false">
           </cfif>
        </div>
        </div>
        </div>
        </div>
    </cfoutput>
    <cfinclude template="footer.cfm">
    </div>
</body>
</html>
