<cfparam name="form.firstname" default="">
<cfparam name="form.lastname" default="">
<cfparam name="form.email" default="">
<cfparam name="form.password" default="">
<cfparam name="form.confirmpassword" default="">
<cfparam name="form.register" default="">

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Register an Account</div>
      <div class="card-body">

        <!--================================ Registration form ==================================-->
        <cfif form.register gt "">

               <cfif form.firstname EQ "">
                Please provide a Name!
               <cfelseif form.lastname EQ "">
                Please provide a Surname!
               <cfelseif form.email EQ "">
                Please provide an Email!
               <cfelseif form.password EQ "">
                Please provide a Password!
               <cfelseif form.confirmpassword EQ "">
                Please provide a Cornfirm Password!
               <cfelseif form.confirmpassword NEQ form.password>
                Your password does not match,Please try again!
               <cfelse>                
                <!--==== Add a new user in the database ========================================-->

                <cfquery name="user" datasource="#application.db#">    
                    SELECT * FROM admin 
                    WHERE EmailAddress = <cfqueryparam cfsqltype="cf_sql_varchar"  value="#form.email#"> 
                     AND Password = <cfqueryparam cfsqltype="cf_sql_varchar"  value="#form.password#">
                </cfquery>

                <!--==== Count number of records in the database ===-->
                  <cfif user.RecordCount gt 0>              
                    The email already exit!, please try! 
                      <cflocation url="login.cfm?Email=#form.Email#" addtoken="false"> 
                  <cfelse>
                    <cfquery name="users" datasource="#application.db#">   
                      INSERT INTO admin(EmailAddress,Password,firstname,surname) 
                        VALUES (<cfqueryparam value="#trim(form.email)#" cfsqltype= "cf_sql_varchar" />, 
                            <cfqueryparam value="#password#" cfsqltype="cf_sql_varchar" />, 
                            <cfqueryparam value="#trim(form.firstname)#" cfsqltype= "cf_sql_varchar" />,  
                            <cfqueryparam value="#trim(form.lastname)#" cfsqltype= "cf_sql_varchar" />)
                    </cfquery>               
                  <cflocation url="login.cfm?Email=#form.Email#" addtoken="false">                           
           </cfif>
        </cfif>

        <form method="POST" action="?">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">First name</label>
                <input class="form-control" id="exampleInputName" name="firstname" type="text" aria-describedby="nameHelp" placeholder="Enter first name">
              </div>
              <div class="col-md-6">
                <label for="exampleInputLastName">Last name</label>
                <input class="form-control" id="exampleInputLastName" name="lastname" type="text" aria-describedby="nameHelp" placeholder="Enter last name">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input class="form-control" id="exampleInputEmail1" name="email" type="email" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPassword1">Password</label>
                <input class="form-control" id="exampleInputPassword1" name="password" type="password" placeholder="Password">
              </div>
              <div class="col-md-6">
                <label for="exampleConfirmPassword">Confirm password</label>
                <input class="form-control" id="exampleConfirmPassword" name="confirmpassword" type="password" placeholder="Confirm password">
              </div>
            </div>
          </div>
          <input type="submit" name="register" class="btn btn-primary btn-block" value="Register" >

        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="login.cfm">Login Page</a>
          <a class="d-block small" href="forgot-password.cfm">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
