<cfparam name="form.btnsave" default="">
<cfinclude template="header.cfm">

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="posts.cfm">Posts</a>
        </li>
        <li class="breadcrumb-item active">New</li>
      </ol>

    <div class="row">
    <div class="col-12">

    <cfif form.btnsave gt "">     
      <cfif len(trim(form.fileUpload))>
          <cftry>
          <cffile action="upload" fileField="fileUpload" accept="image/jpg,image/gif,image/png,bmp,image/jpeg" destination="#application.filepath#/post" nameconflict="makeunique">

             <cfset filename = cffile.serverfile> 
             <cfset datetime = DateFormat(now(), "yyyy-mm-dd")>  
                <cfquery name="post" datasource="#application.db#"> 
                  INSERT INTO posts(AdminID,PostTitle,PostsubTitle,PostDate,PostText,PostImage) 
                      VALUES (<cfqueryparam value="#session.AdminID#" cfsqltype= "cf_sql_varchar" />, 
                              <cfqueryparam value="#form.posttitle#" cfsqltype="cf_sql_varchar" />, 
                              <cfqueryparam value="#form.postsubtitle#" cfsqltype="cf_sql_varchar" />,     
                              <cfqueryparam value="#datetime#" cfsqltype= "cf_sql_varchar" />, 
                              <cfqueryparam value="#form.posttext#" cfsqltype= "cf_sql_varchar" />, 
                              <cfqueryparam value="#filename#" cfsqltype= "cf_sql_varchar" />)
                </cfquery> 
              <cflocation url="posts.cfm" addtoken="false"> 
              <cfset  form.btnsave = ""> 

              <cfcatch type="any">  
                <h3 style="background-color:rgba(255, 99, 71, 0.5);color:white;">Error! Invalid file type.</h3>
              </cfcatch>
          </cftry>
      </cfif>
    </cfif>
      
    <form  method="POST" action="?" enctype="multipart/form-data">

      <h1>New Post </h1> 
        <div class="form-group">
            <div class="form-row"> 
              <div class="col-md-6">
                <label for="exampleInputPostTitle">Title </label> &nbsp; <input class="form-control" id="exampleInputPostTitle" name="posttitle" required="true" type="text" aria-describedby="nameHelp" placeholder="Post title ">
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row"> 
              <div class="col-md-6">
                <label for="exampleInputPostTitle">Post Sub Title </label>
                <input class="form-control" id="exampleInputPostSubTitle" name="postsubtitle" required="true" type="text" aria-describedby="nameHelp" placeholder="Post Sub title">
              </div>
            </div>
          </div>

          <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <label for="exampleInputImage ">Image Upload </label>
                  <input class="form-control" id="exampleInputPostText " name="fileUpload" required="true" type="file" placeholder="Image">
                </div>
              </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputPostText ">Post Text </label>
                <textarea class="form-control" id="exampleInputPostText " name="posttext" required="true" type="text" rows="3" cols="60" placeholder="Enter text here......."></textarea>
              </div>
            </div>
          </div>
          <input type="submit" name="btnsave" class="btn btn-primary" value="Save">  
        </form>
        </div>
      </div>
    </div>
    <cfinclude template="footer.cfm">
