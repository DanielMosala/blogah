<cfparam name="form.btnlogin" default="">
<cfparam name="form.email" default="">
<cfparam name="form.password" default="">
<cfparam name="form.checkbox" default="">
<cfparam name="url..Email" default="">
<cfoutput>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">

		<!--=============================== Login form ==================================-->
    		<cfif form.btnlogin gt "">       
	        
	            <!--==== Check the user in the database ===========-->
		        <cfquery name="users" datasource="#application.db#">    
			        SELECT * FROM admin 
			        WHERE EmailAddress = <cfqueryparam cfsqltype="cf_sql_varchar"  value="#form.email#"> 
			         AND Password = <cfqueryparam cfsqltype="cf_sql_varchar"  value="#form.Password#">
			   	</cfquery>
					<!--==== Count number of records in the database ===-->
			   	<cfif users.RecordCount gt 0>
			   		<cfset session.AdminID = users.AdminID>
			   		<cflocation url="index.cfm" addtoken="false">
			   	<cfelse>
					Invalid login! 
			   	</cfif>		     		
	       </cfif>
      	
        <form method="POST" action="?">
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input class="form-control" id="exampleInputEmail1" name="email" type="email" aria-describedby="emailHelp" placeholder="Enter email" value="#url.Email#">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input class="form-control" id="exampleInputPassword1" name="password" type="password" placeholder="Password">
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Remember Password</label>
            </div>
          </div>
          <input type="submit" class="btn btn-primary btn-block" name="btnlogin" value="Login">
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="register.cfm">Register an Account</a>
          <a class="d-block small" href="forgot-password.cfm">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>
</html>

</cfoutput>



